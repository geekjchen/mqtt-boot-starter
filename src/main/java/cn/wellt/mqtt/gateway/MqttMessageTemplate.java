package cn.wellt.mqtt.gateway;

import org.springframework.integration.annotation.MessagingGateway;
import org.springframework.integration.mqtt.support.MqttHeaders;
import org.springframework.messaging.handler.annotation.Header;

/**
 * mqtt消息推送
 *
 * @author caojingchen
 * @date 2021/2/1 11:20
 */
@MessagingGateway(defaultRequestChannel = "mqttOutboundChannel")
public interface MqttMessageTemplate {

    /**
     * 发送消息
     *
     * @param data  消息
     * @param topic 主题
     */
    void send(String data, @Header(MqttHeaders.TOPIC) String topic);


    /**
     * 发送消息
     *
     * @param data  消息
     * @param topic 主题
     * @param qos   业务质量
     */
    void send(String data, @Header(MqttHeaders.TOPIC) String topic, @Header(MqttHeaders.QOS) int qos);

    /**
     * 发送消息
     *
     * @param data  消息
     * @param topic 主题
     */
    void send(byte[] data, @Header(MqttHeaders.TOPIC) String topic);


    /**
     * 发送消息
     *
     * @param data  消息
     * @param topic 主题
     * @param qos   业务质量
     */
    void send(byte[] data, @Header(MqttHeaders.TOPIC) String topic, @Header(MqttHeaders.QOS) int qos);
}
