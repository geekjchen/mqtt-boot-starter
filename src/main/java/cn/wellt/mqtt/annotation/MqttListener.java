package cn.wellt.mqtt.annotation;

import java.lang.annotation.*;

/**
 * Mqtt消息接收
 *
 * @author caojingchen
 * @date 2021/2/1 11:26
 */
@Retention(RetentionPolicy.RUNTIME)
@Documented
@Target(ElementType.TYPE)
public @interface MqttListener {
    /**
     * 订阅的主题
     */
    String topic();

    /**
     * 消息质量
     * 0 almost once
     * 1 at least once
     * 2 exactly once
     */
    int qos() default 1;
}
