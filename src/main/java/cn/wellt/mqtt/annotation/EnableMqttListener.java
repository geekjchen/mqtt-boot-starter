package cn.wellt.mqtt.annotation;

import cn.wellt.mqtt.config.MqttMessageListenerRegister;
import org.springframework.context.annotation.Import;

import java.lang.annotation.*;

/**
 * 启动器
 *
 * @author caojingchen
 * @date 2021/2/1 11:47
 */
@Retention(RetentionPolicy.RUNTIME)
@Documented
@Target(ElementType.TYPE)
@Import({MqttMessageListenerRegister.class})
public @interface EnableMqttListener {
    String[] basePackages() default {};
}
