package cn.wellt.mqtt.properties;

import org.springframework.boot.context.properties.ConfigurationProperties;

/**
 * mqtt配置
 *
 * @author caojingchen
 * @date 2021/2/1 16:28
 */
@ConfigurationProperties(prefix = "mqtt")
public class MqttConfigProperties {
    /**
     * mqtt server 用户名
     */
    private String username;
    /**
     * mqtt server 密码
     */
    private String password;
    /**
     * mqtt server 地址
     */
    private String host;
    /**
     * 接收消息的客户端id
     */
    private String inboundClientPrefix;
    /**
     * 发送消息的客户端id
     */
    private String outboundClientPrefix;
    /**
     * 发送消息默认主题
     */
    private String defaultTopic;
    /**
     *
     */
    private int completionTimeout = 5000;
    private int timeout = 10;

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getHost() {
        return host;
    }

    public void setHost(String host) {
        this.host = host;
    }

    public String getInboundClientPrefix() {
        return inboundClientPrefix;
    }

    public void setInboundClientPrefix(String inboundClientPrefix) {
        this.inboundClientPrefix = inboundClientPrefix;
    }

    public String getOutboundClientPrefix() {
        return outboundClientPrefix;
    }

    public void setOutboundClientPrefix(String outboundClientPrefix) {
        this.outboundClientPrefix = outboundClientPrefix;
    }

    public String getDefaultTopic() {
        return defaultTopic;
    }

    public void setDefaultTopic(String defaultTopic) {
        this.defaultTopic = defaultTopic;
    }

    public int getCompletionTimeout() {
        return completionTimeout;
    }

    public void setCompletionTimeout(int completionTimeout) {
        this.completionTimeout = completionTimeout;
    }

    public int getTimeout() {
        return timeout;
    }

    public void setTimeout(int timeout) {
        this.timeout = timeout;
    }
}
