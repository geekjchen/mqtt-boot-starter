package cn.wellt.mqtt.callback;

import org.eclipse.paho.client.mqttv3.MqttMessage;

/**
 * 消息接收
 *
 * @author caojingchen
 * @date 2021/2/1 11:34
 */
public interface MqttMessageListener {

    /**
     * 收到消息
     *
     * @param topic   消息主题
     * @param message 消息内容
     */
    void onMessage(String topic, MqttMessage message);
}
