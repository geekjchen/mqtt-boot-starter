# mqtt-boot-starter

#### 介绍
{使用Spring框架自带的消息模块和mqtt模块实现}



#### 安装教程

引入即可

#### 使用说明

1.  发送消息 `MqttMesssageTemplate`
2.  接收 
    1. 在配置类上添加 `@EnableMqttListener`
    2. 编写消息处理类 继承 `MqttMessageListener`,并添加`@MqttListener`指定订阅的主题和qos(主题支持#和+)

